<?php

return [
    'facebook' => [
        'appId' => $_ENV['FB_APP_ID'],
        'secret' => $_ENV['FB_APP_SECRET'],
    ],
];