<?php

$factory('App\Category', [
    'name' => $faker->unique()->word,
    'description' => $faker->sentence,
    'image_url' => 'http://www.placehold.it/400x300'
]);

$factory('App\Subcategory', [
    'name' => $faker->unique()->word,
    'description' => $faker->sentence,
    'image_url' => 'http://www.placehold.it/400x300',
    'category_id' => $faker->numberBetween(1,10)
]);

$factory('App\User', [
    'fname' => $faker->firstName,
    'lname' => $faker->lastName,
    'email' => $faker->unique()->email,
    'password' => "P@ssword",
]);