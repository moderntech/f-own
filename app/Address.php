<?php namespace App;

use Illuminate\Database\Eloquent\Model;


class Address extends Model {

    protected $fillable = ['user_id', 'line1', 'line2', 'city', 'state', 'zip', 'plus4'];

	public function User() {
        return $this->belongsTo('App\User');
    }

}
