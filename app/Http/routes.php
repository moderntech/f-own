<?php
session_start();

Route::controller('register', 'RegisterController');
Route::controller('auth', 'AuthController');

Route::get('/logout', function() {
   \Auth::logout();
    return \View::make('home')->with('message', 'You have been logged out.');
});

Route::get('/', function() {
    return \View::make('home');
});