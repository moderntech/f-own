<?php namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests;
use App\Services\FBHelper;
use App\Services\Utility;
use App\Services\ValidatorTrait;
use App\User;

use Facebook\FacebookRequest;
use Facebook\FacebookSDKException;
use Facebook\FacebookSession;
use Facebook\GraphUser;

use Illuminate\Database\QueryException;
use Illuminate\Support\MessageBag;

class RegisterController extends Controller {
use ValidatorTrait;

    protected $rules = [
        'fname' => 'required|min:2',
        'lname' => 'required|min:2',
        'email' => 'required|email|unique:users',
        'password' => 'required|confirmed|regex:^((?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9!@#$%&*]{6,20})$^'
    ];

    protected $messages = [
        'regex' => "Your password must be between 6 and 20 characters in length, and be made up of a combination of letters and numbers. For enhanced security, you can also use any of the following symbols: ! @ # $ % *"
    ];

    public function createUser($input) {
        try {
            return User::create([
                'fname'    => $input['fname'],
                'lname'    => $input['lname'],
                'email'    => $input['email'],
                'password' => bcrypt($input['password']),
                'fb_token' => $input['fb_token'],
                'fb_id'    => $input['fb_id']
            ]);
        }
        catch (QueryException $e) {
            return FALSE;
        }
    }

	public function getIndex()
	{
        $fb = new \App\Services\FBHelper();
        $fbLoginUrl = $fb->getLoginUrl('http://fathersown.dev/register/fbcallback', ['email']);
        $cats = Category::all();
		return \View::make('auth.register')->with([
            'cats' => $cats,
            'fb_login_url' => $fbLoginUrl,
        ]);
	}

    public function getFbcallback() {
        $facebook = new FBHelper;
        try {
            if ($fb_session = $facebook->getSessionFromRedirect('http://fathersown.dev/register/fbcallback')) {
                if ($fb_session instanceof FacebookSession) {
                    $_SESSION['fb_session'] = $fb_session;
                    $user_profile = (new FacebookRequest($fb_session, 'GET', '/me'))->execute()->getGraphObject(GraphUser::className());

                    $id = $user_profile->getId();
                    if($user = Utility::fb_id_exists($id)) {
                        if($user->confirmed) {
                            \Auth::loginUsingId($user->id);;
                            return \View::make('home')->with('message', 'You have already registered, so we logged you in instead :)');
                        }
                        else {
                            return \View::make('home')->with('message', 'You have already registered, but your account has not yet been confirmed. Please check your e-mail for your confirmation message.');
                        }
                    }
                    $photo_url = "https://graph.facebook.com/$id/picture?type=square";
                    $fname = $user_profile->getFirstName();
                    $lname = $user_profile->getLastName();
                    $email = $user_profile->getEmail();
                    return \View::make('auth.register')->with([
                        'fb_register' => TRUE,
                        'fname' => $fname,
                        'lname' => $lname,
                        'email' => $email,
                        'photo_url' => $photo_url,
                        'fb_token' => $fb_session->getAccessToken()->__toString(),
                        'fb_id' => $id
                    ]);
                }
                else {
                    // something went wrong
                    $fb_error = new MessageBag([$fb_session]);
                    return \View::make('auth.register')->with('fb_err', $fb_error);
                }
            }
        }
        catch (FacebookSDKException $e) {
            $fb_error = new MessageBag([$e->getMessage()]);
            return \View::make('auth.register')->with('fb_err', $fb_error);
        }

    }

    public function postIndex() {
        $validated_input = \Input::all();
        if($failures = $this->validatorFails($validated_input)) {
            return \Redirect::to('register')->withInput($validated_input)->withErrors($failures);
        } else {
            if ($user = $this->createUser(\Input::all())) {
                return \View::make('auth.success')->with('user', $user);
            } else {
                return "Error encountered. Contact system administrator.";
            }

        }
    }

}


