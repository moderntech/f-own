<?php namespace App\Http\Controllers;

use App\Services\ValidatorTrait;

class AuthController extends Controller {
    use ValidatorTrait;

    protected $rules = [
       'email' => 'required',
       'password' => 'required'
    ];

    public function getIndex() {
        return \View::make('auth.login');
    }

    public function postIndex() {
        $validated_input = \Input::all();
        if ($failures = $this->validatorFails($validated_input, [])) {
            return \Redirect::to('auth')->withErrors($failures);
        }
        else {
            dd($validated_input);
            // TODO: validate credentials
            // TODO: login user
            // TODO: redirect to home / dashboard
        }
    }

}