<?php namespace App\Services;

use \Facebook\FacebookSession;
use \Facebook\FacebookRedirectLoginHelper;
use \Facebook\FacebookRequest;
use \Facebook\GraphUser;
use \Facebook\GraphObject;
use \Exception;

/**
 * Helper class to initialize Facebook SDK v4.0
 *
 * @package App\Services
 * @author Jason Swint
 **/
class Facebook
{

    /**
     * Initialize "Facebook Helper" object
     *
     * @return void
     * @author Jason Swint
     **/
    public function __construct($callbackURL)
    {
        // pull info from config
        $this->callbackURL = $callbackURL;
        $fbConfig  		   = \Config::get('social.facebook');
        $appId     		   = $fbConfig['appId'];
        $appSecret 		   = $fbConfig['secret'];
        // set application parameters
        FacebookSession::setDefaultApplication($appId, $appSecret);
        $this->session = $this->getSession($callbackURL);
        return $this->session;
    }

    /**
     * Get Facebook Login URL
     *
     * @return string
     * @author Jason Swint
     **/
    public function getLoginURL($scope=array())
    {
        $helper = new FacebookRedirectLoginHelper($this->callbackURL);
        return $helper->getLoginUrl($scope);
    }

    /**
     * Attempt to acquire FacebookSession
     * Error handling must be handled by calling script.
     *
     * @return void
     * @author Jason Swint
     **/
    public function getSession($callbackURL)
    {
        $helper = new FacebookRedirectLoginHelper($callbackURL);
        $session = $helper->getSessionFromRedirect();
        return $session;
    }

    /**
     * Return user profile as an array.
     *
     * @return array
     * @author Jason Swint
     **/
    public function getUserProfile()
    {
        if ($this->session instanceof FacebookSession) {
            $user_profile = (new FacebookRequest(
                $this->session, 'GET', '/me'
            ))->execute()->getGraphObject(GraphUser::className());
            return $user_profile->asArray();
        }
        else {
            throw new Exception('Could not acquire session. Unable to load Facebook profile.');
        }
    }

    public function getGraphObjectAsArray($request) {
        if ($this->session instanceof FacebookSession) {
            $graph_object = (new FacebookRequest(
                $this->session, 'GET', $request
            ))->execute()->getGraphObject(GraphObject::className());
            return $graph_object->asArray();
        }
    }
} // END class Facebook

class Helper {
    public static function pretty_dump(array $array) {
        echo "<pre>"; echo var_dump($array);
        echo "</pre>";
    }
} // END class Helper

