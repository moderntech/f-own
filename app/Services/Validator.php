<?php namespace App\Services;

/**
 * Class ValidatorTrait
 * Include this trait in all form controllers which require validation.
 * Note that a $rules property (array) is defined within the class.
 *
 * @package App\Services
 */
trait ValidatorTrait {

    /**
     * Return false if validator passes, otherwise pass back message
     * bag of validation errors.
     *
     * @param array $input User input to be validated
     *
     * @author Jason Swint
     * @return bool|\Illuminate\Support\MessageBag
     */
    protected function validatorFails($input) {
        $validator = \Validator::make($input, $this->rules, $this->messages = []);
        if($validator->fails()) {
            return $validator->messages();
        } else {
            return false;
        }
    }
}

