<?php namespace App\Services;

use App\User;

class Utility {
    static function fb_id_exists($id) {
        if(User::where('fb_id', $id)->count() > 0) {
            return (User::where('fb_id', $id)->first());
        }
        else {
            return FALSE;
        }
    }
}
