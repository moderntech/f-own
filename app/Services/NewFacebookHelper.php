<?php namespace App\Services;

use Facebook\FacebookRequestException;
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;

class FBHelper {
    protected $appId;
    protected $secret;
    protected $accessToken;
    protected $session;

    function __construct() {
        $fbConfig = \Config::get('social.facebook', null);
        if (is_null($fbConfig) or empty($fbConfig['appId'])or empty($fbConfig['secret'])) {
            throw new FBHelperException('Check configuration at config/social.php. See documentation for details.');
        }
        $this->appId = $fbConfig['appId'];
        $this->appSecret = $fbConfig['secret'];
        FacebookSession::setDefaultApplication($this->appId, $this->appSecret);
    }

    function getLoginUrl($callbackUrl, array $scope=[]) {
        $helper = new FacebookRedirectLoginHelper($callbackUrl);
        return $helper->getLoginUrl($scope);
    }

    function getSessionFromRedirect($callbackUrl) {
        $helper = new FacebookRedirectLoginHelper($callbackUrl);
        try {
            $this->session = $helper->getSessionFromRedirect();
            if ($this->session instanceof FacebookSession) {
                return($this->session);
            }
            else {
                return FALSE;
            }
        }
        catch(FacebookRequestException $ex) {
            return $ex->getCode() . ': ' . $ex->getMessage();
        }
    }

    function getSessionWithToken($access_token) {
        $this->$session = new FacebookSession($access_token);
    }

    function setAccessToken($access_token) {
        $this->accessToken = $access_token;
    }
}


class FBHelperException extends \Exception {

}