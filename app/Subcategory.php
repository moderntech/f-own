<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model {

	protected $fillable = ['name', 'description', 'photo_url'];

    public function Category() {
        return $this->belongsTo('App\Category');
    }

}
