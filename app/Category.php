<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $fillable = ['name', 'description', 'photo_url'];

    public function Subcategories() {
        return $this->hasMany('App\Subcategory');
    }

}
