<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users', function(Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('lname');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('fb_id')->unique()->nullable()->default(null);
            $table->string('fb_token')->nullable()->default(null);;
            $table->boolean('confirmed')->default(0);
            $table->boolean('suspended')->default(0);
            $table->string('remember_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
	public function down()
	{
		Schema::drop('users');
	}

}
