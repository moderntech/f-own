@extends('app')
@section('subtitle')
    - Thanks for signing up!
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 col-md-offset-1 hidden-sm hidden-xs">
            <img src="{{asset('img/grace.jpg')}}" alt="Grace" width="200px"/>
            <p>Hi! I'm Grace. I'll pop in from time to time to provide helpful hints and tips. We're so glad that you could join us!</p>
        </div>
        <div class="col-md-7">
            <div class="jumbotron" style="padding:20px">
                <h2>Wow! That was easy!</h2>
                <p>Now just kick back and relax, {{$user->fname}}. We'll send you a confirmation e-mail in just a minute or two. Once you click on the activation link in that e-mail, you'll be a bona-fide member here at Father's Own, and we'll have a special gift waiting for you, too!</p>
            </div>
        </div>
    </div>
@endsection