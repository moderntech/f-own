@extends('app')
@section('subtitle')
    - User Registration - Sign Up For an Account
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
        <div class="col-md-4 col-md-offset-1">
           <img src="{{{ asset('img/grace.jpg') }}}" style="float:left; padding-right: 10px" width="100px" height="130px" />
            <p style="font-size: 1.4em;">Registration with Father's Own is fast, easy, and free! We promise to never share or sell your personal information.</p>
            <div class="alert alert-info" role="alert" style="clear: both; font-size: 1.2em">We don't require registration to browse or purchase items, but you'll need an account with us, as well as a <a href="https://www.paypal.com">PayPal</a> account to post items for sale.</div>
        </div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">Register</div>
				<div class="panel-body">
                    @if (isset($fb_err))
                        @if ($fb_err->count() > 0)
                            <div class="alert alert-danger">
                                <strong>Something went wrong while communicating with Facebook!</strong>
                                <div class="small">
                                    <ul>
                                        @foreach($fb_err->getMessages() as $message)
                                            <li>{{ $message[0][0] }}</li>
                                        @endforeach
                                    </ul>
                                    <p>If you believe that this may be an issue with our website, you can report this error
                                    to the webmaster. Otherwise, you are welcome to register with us using the form below:</p>
                                </div>
                            </div>
                        @endif
                    @endif
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Uh-Oh!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">First Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="fname" value=
								@if(isset($fname))
								   "{{ $fname }}">
								@else
                                    "{{ old('fname') }}">
                                @endif
							</div>
						</div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Last Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="lname" value=
                                @if(isset($lname))
                                "{{ $lname }}">
                                @else
                                    "{{ old('lname') }}">
                                @endif
                            </div>
                        </div>


						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value=
                                @if(isset($email))
                                "{{ $email }}">
                                @else
                                    "{{ old('email') }}">
                                @endif
							</div>
						</div>
                        <input type="hidden" name="fb_token" value=
                        @if(isset($fb_token))
                        "{{ $fb_token }}"
                        @else
                            "{{ old('fb_token') }}"
                        @endif
                        />
                        <input type="hidden" name="fb_id" value=
                        @if(isset($fb_id))
                            "{{ $fb_id }}"
                        @else
                            "{{ old('fb_id') }}"
                        @endif
                        />

                        @if(isset($fb_register))
                            <div class='alert alert-info' id='fb-login-info'>
                                <div style='font-size: 2em'><span class='glyphicon glyphicon-thumbs-up'></span> Splendid!<img style="float:right; border:solid black 1px; border-radius: 40px;" height='40px' src="{{ $photo_url }}" alt="Facebook Profile Photo"/></div>
                                <div style="clear:both">We've imported some of your information from Facebook, but we still need you to set up a password to login to our site, should you ever choose to close your Facebook account.</div>
                            </div>
                        @else
                            <hr />
                        @endif
						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Register
								</button>
                                <button type="clear" class="btn">
                                    Clear Form
                                </button>
							</div>
						</div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                 By clicking the "Register" button above, you indicate that you have read and agree to the <a href="#">User Agreement</a> and <a href="#">Privacy Policy</a>.
                            </div>
                        </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
