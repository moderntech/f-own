<?php $cats = \App\Category::all();

$fb = new \App\Services\FBHelper();
$fb_login_url = $fb->getLoginUrl('http://fathersown.dev/register/fbcallback');

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Father's Own
    @section('subtitle')
    @show
    </title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

<nav class="navbar navbar-default" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/">Father's Own</a>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav">
			<li><a href="#">Trending</a></li>
			<li><a href="#">Top Sellers</a></li>
		</ul>
		<form class="navbar-form navbar-left" role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
			<button type="submit" class="btn btn-default">Submit</button>
		</form>
		<ul class="nav navbar-nav navbar-right">
            @if (\Auth::check())
                <li><a href="/logout">Log Out</a></li>
            @else
                <li><a href="#">Log In</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Register <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        @if(isset($fb_login_url))
                            <li><a href={{ $fb_login_url }}>Sign up with Facebook</a></li>
                        @endif
                        <li><a href="/register">Register</a></li>
                    </ul>
                </li>
            @endif
		</ul>
	</div><!-- /.navbar-collapse -->
</nav>
@if(isset($message))
    <div class="alert alert-info">{{$message}}</div>
    @endif

	@yield('content')

<nav class="navbar navbar-default navbar-fixed-bottom hidden-sm hidden-xs" role="navigation">
	<a class="navbar-brand" href="#">Browse Categories:</a>
    <ul class="nav navbar-nav navbar-left">
        {{-- Logic to display categories/subcategories --}}
        @foreach($cats as $cat)
            @if ($cat->Subcategories->count() > 0)
                <?php $subcats = $cat->Subcategories; ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" title="{{$cat->description}}" data-toggle="dropdown">{{$cat->name}}<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    @foreach($subcats as $subcat)
                     <li><a href="#" title="{{$subcat->description}}">{{$subcat->name}}</a></li>
                    @endforeach
                    </ul>
                 </li>
            @else
                <li><a href="#">{{$cat->name}}</a></li>
            @endif
        @endforeach
    </ul>
</nav>

	<!-- Scripts -->

	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

</body>
</html>
